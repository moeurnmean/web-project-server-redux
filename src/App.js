import logo from "./logo.svg";
import "./App.css";
import AllUsers from "./pages/AllUsers";
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <div>
      <AllUsers />
    </div>
  );
}

export default App;
