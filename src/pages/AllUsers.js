import axios from "axios";
import React from "react";
import { useEffect, useState } from "react";
import { FETCH_ALL_USERS } from "../service/userService";

const AllUsers = () => {
  const [allUsers, setAllUsers] = useState([]);
  // fetch user -> api axios
  // reponse -> list down card
  useEffect(() => {
    FETCH_ALL_USERS()
      .then((res) => setAllUsers(res))
      .catch((err) => console.log("Error : " + err));
  }, []);

  console.log("allUsers are : ", allUsers);

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-4">
            <h1> here are the users </h1>
          </div>
        </div>
      </div>
    </>
  );
};

export default AllUsers;
